package ekauppa;

import java.util.Set;

public class Ekauppa {

    private Ostoskori ostoskori;
    private Varasto varasto;

    public Ekauppa(Ostoskori kori, Varasto varasto) {
        this.ostoskori = kori;
        this.varasto = varasto;
    }

    public String[] vaihtoehdot() {
        return this.varasto.vaihtoehdot();
    }

    public void tilaus(int indeksi, int kpl) {
        String tuotteenNimi = this.vaihtoehdot()[indeksi];
        Tuote tilattu = this.varasto.haeTuoteNimella(tuotteenNimi);
        this.ostoskori.ostaTuote(tilattu, kpl);
    }

    public String varastotilanne() {
        return this.varasto.toString();
    }

    public double kokonaishinta() {
        return this.ostoskori.kokonaishinta();
    }

    public String korinTilanne() {
        return this.ostoskori.toString();
    }

    public String puuttuvatTilaukset() {
        return this.ostoskori.puuttuvat();
    }

    public Set<Tuote> tallennettavatVarastosta() {
        return this.varasto.tallennettavatVarastosta();
    }

    public Set<Tuote> tallennettavatKorista() {
        return this.ostoskori.tallennettavatKorista();
    }

    public Set<Tuote> tallennettavatPuuttuvista() {
        return this.ostoskori.tallennettavatPuuttuvista();
    }

}
