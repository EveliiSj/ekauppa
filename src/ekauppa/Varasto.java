package ekauppa;

import java.util.*;

public class Varasto {

    private Map<Tuote, Integer> maarat;

    public Varasto() {
        // Varastossa olevien tuotteiden määrät
        this.maarat = new HashMap<>();
    }

    public void lisaaTuote(Tuote tuote) {
        // lisätään tuote varastoon 
        // korvaa aiemmat esiintymät
        maarat.put(tuote, tuote.getMaara());
    }

    public int maara(Tuote tuote) {
        // haetaan kuinka monta kpl on varastossa
        if (maarat.containsKey(tuote)) {
            return maarat.get(tuote);
        }
        return 0;
    }

    public boolean ota(Tuote tuote, int kpl) {
        // jos tuote löytyy varastosta ja saldoa voi vähentää, 
        // vähennetään ja palautetaan true
        if (maarat.containsKey(tuote) && maarat.get(tuote) - kpl >= 0) {
            // vähennetään varastossa olevaa määrää
            maarat.put(tuote, maarat.get(tuote) - kpl);
            // vähennetään tuotteen määrää
            tuote.setMaara(tuote.getMaara() - kpl);
            return true;
        }
        // jos ei löydy tai menisi alle nollan, palautetaan false
        return false;
    }

    // tehdään lista tilattavissa olevien tuotteiden nimistä 
    public String[] vaihtoehdot() {
        int koko = this.maarat.size();
        String[] nimet = new String[koko];
        int i = 0;
        for (Map.Entry<Tuote, Integer> tavara : this.maarat.entrySet()) {
            nimet[i] = tavara.getKey().getNimi();
            i++;
        }
        return nimet;
    }
    
    // haetaan Tuote-olio varastosta nimen perusteella
    public Tuote haeTuoteNimella(String nimi) {
        Tuote haettu = null;
        for (Map.Entry<Tuote, Integer> tavara : this.maarat.entrySet()) {
            if (tavara.getKey().getNimi().equals(nimi)) {
                haettu = tavara.getKey();
            }
        }
        return haettu;
    }
    
    // tehdään Set varastossa olevista tuotteista tallentamista varten
    public Set<Tuote> tallennettavatVarastosta() {
        return this.maarat.keySet();
    }
    
    
    
    @Override
    public String toString() {
        String tulostus = "";
        for (Map.Entry<Tuote, Integer> tavara : this.maarat.entrySet()) {
            tulostus += "Tuotetta " + tavara.getKey().getNimi() + " (" + tavara.getKey().getKoodi() + ") varastossa " + maarat.get(tavara.getKey()) + " kpl. \n";
        }
        return tulostus;
    }

}
