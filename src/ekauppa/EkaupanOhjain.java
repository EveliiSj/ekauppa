package ekauppa;

public class EkaupanOhjain {

    private Ekauppa malli;
    private EkaupanGUI nakyma;
    private Tallennus tallennus;

    public EkaupanOhjain(Ekauppa malli, EkaupanGUI nakyma) {
        this.malli = malli;
        this.nakyma = nakyma;
        this.tallennus = new Tallennus();
    }
    
    public void teeTilaus() {
        int indeksi = nakyma.getTuotteenIndeksi();
        int maara = nakyma.getMaara();
        malli.tilaus(indeksi, maara);
        double hinta = malli.kokonaishinta();
        nakyma.asetaHinta(hinta);
       
        nakyma.asetaTekstiVarastolle(malli.varastotilanne());  
        nakyma.asetaTekstiKorille(malli.korinTilanne());
        nakyma.asetaTekstiPuuttuville(malli.puuttuvatTilaukset());

    }

    public void tallenna(boolean uusi, String valinta) {
        if (valinta.equals("puuttuvat")) {
            tallennus.tallenna(uusi, this.malli.tallennettavatPuuttuvista());
        } else if (valinta.equals("kori")) {
            tallennus.tallenna(uusi, this.malli.tallennettavatKorista());
        } else if (valinta.equals("varasto")) {
            tallennus.tallenna(uusi, this.malli.tallennettavatVarastosta());
        }
    }

    public void lopeta() {
        System.exit(0);
    }

    public static void main(String[] args) {

        Varasto varasto = new Varasto();
        Ostoskori kori = new Ostoskori(varasto);

        // laitetaan varastoon tuotteita
        Tuote ananas = new Tuote("Ananas", "A01", 5, 1.5);
        Tuote mango = new Tuote("Mango", "M01", 6, 1.7);
        Tuote papaija = new Tuote("Papaija", "P01", 1, 2.1);
        Tuote persikka = new Tuote("Persikka", "P02", 10, 0.9);
        Tuote omena = new Tuote("Omena", "O01", 15, 1.1);
        Tuote guava = new Tuote("Guava", "G01", 3, 3.9);
        Tuote persimon = new Tuote("Persimon", "P03", 2, 0.8);
        Tuote lime = new Tuote("Lime", "L01", 22, 0.6);
        Tuote sitruuna = new Tuote("Sitruuna", "S01", 13, 1.45);

        varasto.lisaaTuote(ananas);
        varasto.lisaaTuote(mango);
        varasto.lisaaTuote(papaija);
        varasto.lisaaTuote(persikka);
        varasto.lisaaTuote(omena);
        varasto.lisaaTuote(guava);
        varasto.lisaaTuote(persimon);
        varasto.lisaaTuote(lime);
        varasto.lisaaTuote(sitruuna);

        Ekauppa malli = new Ekauppa(kori, varasto);
        
        // välitetään tarvittavat tiedot mallitasolta näkymälle 
        EkaupanGUI nakyma = new EkaupanGUI(malli.korinTilanne(), malli.varastotilanne(), malli.puuttuvatTilaukset(), malli.vaihtoehdot());

        EkaupanOhjain ohjain = new EkaupanOhjain(malli, nakyma);

        nakyma.rekisteroiOhjain(ohjain);

    }

}
