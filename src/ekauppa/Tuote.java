

package ekauppa;

// tallentamista varten luokan on toteutettava rajapinta Serializable
import java.io.Serializable;

public class Tuote implements Serializable {
    
    private String nimi;
    private String koodi;
    private int maara;
    private double hinta;
    
    public Tuote(String nimi, String koodi, int maara, double hinta) {
        this.nimi = nimi;
        this.koodi = koodi;
        this.maara = maara;
        this.hinta = hinta;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    public void setKoodi(String koodi) {
        this.koodi = koodi;
    }

    public void setMaara(int maara) {
        this.maara = maara;
    }

    public void setHinta(double hinta) {
        this.hinta = hinta;
    }

    public String getNimi() {
        return nimi;
    }

    public String getKoodi() {
        return koodi;
    }

    public int getMaara() {
        return maara;
    }

    public double getHinta() {
        return hinta;
    }
    
    @Override
    public String toString() {
        return "Tuote " + this.getNimi() + " (" + this.getKoodi() + ") \n"
                + this.getHinta() + " e/kpl \n"
                + "Varastossa " + this.getMaara() + " kpl.";
                
    }

}
