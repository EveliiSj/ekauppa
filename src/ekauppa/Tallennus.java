package ekauppa;

import java.io.*;
import java.util.*;

public class Tallennus implements Serializable {
    private File tiedosto;
    
    public Tallennus() {
        this.tiedosto = new File("tiedot.dat");
    }

    public void tallenna(boolean uusi, Set<Tuote> tallennettavat) {
        try {
            // luodaan uusi oliovirta
            // jos boolean uusi = true, niin ylikirjoitetaan tiedosto
            // jos boolean uusi = false, niin kirjoitetaan dataa tiedoston loppuun 
            FileOutputStream out = new FileOutputStream(this.tiedosto, !uusi);
            ObjectOutputStream tuloste = new ObjectOutputStream(out);
            
            // kirjoitetaan kaikki tuotteet setistä tallennettavat tiedostoon
            for (Tuote tavara : tallennettavat) {
                tuloste.writeObject(tavara);
            }

            // suljetaan tiedosto
            tuloste.close();
            
            // varaudutaan virheisiin
        } catch (FileNotFoundException e) {
            System.out.println("Tiedostoa ei löydy.");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Tiedostoon kirjoittamisessa tapahtui virhe.");
        } catch (Exception e) {
            System.out.println("Ohjelmassa tapahtui virhe.");
        }
    }

}
