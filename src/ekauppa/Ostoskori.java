package ekauppa;

import java.util.*;

public class Ostoskori {

    // kori tilauksille
    private Map<Tuote, Integer> kori;
    // kori tuotteille, joita ei ole varastossa
    private Map<Tuote, Integer> puuttuvat;
    // tilattavissa olevat tuotteet ovat tässä varastossa
    private Varasto varasto;

    public Ostoskori(Varasto varasto) {
        this.kori = new HashMap<>();
        this.puuttuvat = new HashMap<>();
        this.varasto = varasto;
    }

    public void ostaTuote(Tuote tuote, int kpl) {

        // tarkistetaan, onko tuotetta tarpeeksi varastossa
        if (varasto.ota(tuote, kpl)) {
            // jos tuote on jo korissa, kasvatetaan vain määrää
            if (this.kori.containsKey(tuote)) {
                this.kori.put(tuote, kori.get(tuote) + kpl);
            } else {
                // tuotetta ei ole vielä korissa, laitetaan sinne
                this.kori.put(tuote, kpl);
            }
        } else {
            // jos tuotetta ei ole tarpeeksi varastossa, tilataan 
            // saatavilla oleva määrä
            int lisays = kpl - varasto.maara(tuote);
            ostaTuote(tuote, varasto.maara(tuote));
            // jos tuote löytyy jo puuttuvien tilausten listalta, 
            // kasvatetaan määrää
            if (this.puuttuvat.containsKey(tuote)) {
                this.puuttuvat.put(tuote, puuttuvat.get(tuote) + Math.max(1, lisays));
            } else {
                this.puuttuvat.put(tuote, Math.max(1, lisays));
            }
        }
    }

    public double kokonaishinta() {
        double total = 0;
        for (Map.Entry<Tuote, Integer> ostos : this.kori.entrySet()) {
            total += ostos.getKey().getHinta() * ostos.getValue();
        }
        return total;
    }

    @Override
    public String toString() {
        String tulos = "Ostoskorissa on \n";
        for (Map.Entry<Tuote, Integer> ostos : this.kori.entrySet()) {
            tulos += ostos.getKey().getNimi() + " (" + ostos.getValue() + " kpl).\n";
        }
        return tulos;
    }

    // tehdään set kaikista ostoskorin tuotteista 
    public Set<Tuote> tallennettavatKorista() {
        return this.kori.keySet();
    }

    // tehdään set kaikista puuttuvista tuotteista 
    public Set<Tuote> tallennettavatPuuttuvista() {
        return this.puuttuvat.keySet();
    }

    public String puuttuvat() {
        String tulos = "Myöhemmin toimitettavat tilaukset: \n";
        for (Map.Entry<Tuote, Integer> ostos
                : this.puuttuvat.entrySet()) {
            tulos += ostos.getKey().getNimi() + " (" + ostos.getValue() + " kpl).\n";
        }
        return tulos;
    }
}
