package ekauppa;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;

public class EkaupanGUI extends JFrame {

    private EkaupanOhjain ohjain;

    // pääpaneeli ja välilehtipaaneli
    private JPanel sisaltopaneeli;
    private JTabbedPane valilehtipaneeli;

    // sisältöpaneelit
    private JPanel varastopaneeli, ostoskoripaneeli, tilauspaneeli, tallennuspaneeli;

    // lisää paneeleita tilauspaneelin sisään
    private JPanel tuotepaneeli, tekstipaneeli;

    // varastopaneeliin tekstiä varten 
    private JScrollPane varastotilanne;
    private String varastoteksti;
    private JTextArea tekstiVarastolle;

    // varastopaneelin kuvaa varten
    private JLabel kuva;

    // tuotelistaa varten 
    private JLabel tekstiTuotteille;
    private JList<String> tuotteet;
    private String[] tilattavat;

    // tilauspaneelin kuvaa varten
    private JLabel kuva2;

    // asiakkaan syöttämää tuotemäärää varten
    private JTextField maara;
    private JLabel tekstiMaaralle;

    // kokonaishinnan näyttämistä varten
    private JTextField kokonaishinta;
    private JLabel tekstiHinnalle;

    // tehdään nappi ostosten tekemiselle
    private JButton tilaa;

    // ostoskorin sisällön näyttämistä varten 
    private String korinSisalto;
    private String puuttuvat;
    private JTextArea tekstiKorille;
    private JTextArea tekstiPuuttuville;
    private JScrollPane korinTilanne;
    private JScrollPane puuttuvienTilanne;

    // tallennuspaneelia varten 
    private JCheckBox puuttuu, kori, varasto, uusi, vanha;
    private JButton tallenna;
    private JPanel valintapaneeli, tiedostopaneeli;

    // valikkoriviä varten
    private JMenuBar valikkorivi;
    private JMenu valikko;
    private JMenuItem lopetus;
    
    public EkaupanGUI(String alkuKori, String alkuVarasto, String alkuPuuttuvat, String[] tuotelista) {
        alustaKomponentit(alkuKori, alkuVarasto, alkuPuuttuvat, tuotelista); 
    }
    
    private void alustaKomponentit(String alkuKori, String alkuVarasto, String alkuPuuttuvat, String[] tuotelista) {
        setTitle("E-kauppa");
        Border kehys = BorderFactory.createEtchedBorder();
        Border tuoteKehys = BorderFactory.createTitledBorder(kehys, "tuotteet");
        Border tallennusKehys = BorderFactory.createTitledBorder(kehys, "Mitä tallennetaan");
        Border fileKehys = BorderFactory.createTitledBorder(kehys, "Mikä tiedosto valitaan");
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        sisaltopaneeli = new JPanel();
        // tehdään välilehtipaneelit
        valilehtipaneeli = new JTabbedPane();
        varastopaneeli = new JPanel();
        tilauspaneeli = new JPanel(new BorderLayout());
        ostoskoripaneeli = new JPanel();
        tallennuspaneeli = new JPanel();

        varastopaneeli.add(new JLabel("Varastotilanne"));
        tilauspaneeli.add(new JLabel("Tilaus"));
        ostoskoripaneeli.add(new JLabel("Ostoskori"));

        valilehtipaneeli.addTab("Tilaus", tilauspaneeli);
        valilehtipaneeli.addTab("Varasto", varastopaneeli);
        valilehtipaneeli.addTab("Ostoskori", ostoskoripaneeli);
        valilehtipaneeli.addTab("Tallennus", tallennuspaneeli);

        /* 
         VARASTOPANEELI
         */

        varastoteksti = alkuVarasto;
        tekstiVarastolle = new JTextArea(varastoteksti, 20, 20);
        varastotilanne = new JScrollPane(tekstiVarastolle);

        // lisätään kuva
        kuva = new JLabel(new ImageIcon(getClass().getResource("/images/fruits.jpg")));

        varastopaneeli.add(varastotilanne);
        varastopaneeli.add(kuva);

        /*
         TILAUSPANEELI
         */
        // näytetään lista tilattavista tuotteista
        tilattavat = tuotelista;
        tuotteet = new JList<>(tilattavat);
        tuotteet.setBorder(tuoteKehys);
        tekstiTuotteille = new JLabel("Tilattavissa: ");
        tekstiTuotteille.setLabelFor(tuotteet);

        // tehdään tekstilaatikko tuotteiden määrän syöttämistä varten
        maara = new JTextField(10);
        tekstiMaaralle = new JLabel("Kappalemäärä");
        tekstiMaaralle.setLabelFor(maara);

        // tehdään tekstilaatikko tilauksen hintaa varten
        kokonaishinta = new JTextField(10);
        tekstiHinnalle = new JLabel("Ostoskorin hinta: ");
        tekstiHinnalle.setLabelFor(kokonaishinta);

        // nappi tilauksen tekemistä varten
        tilaa = new JButton("tilaa");

        // lisätään kuva
        kuva2 = new JLabel(new ImageIcon(getClass().getResource("/images/fruits2.jpg")));

        tuotepaneeli = new JPanel();
        tekstipaneeli = new JPanel();

        // lisätään komponentit sisältöpaneeliin
        tuotepaneeli.add(tuotteet);

        tekstipaneeli.add(tekstiMaaralle);
        tekstipaneeli.add(maara);
        tekstipaneeli.add(tekstiHinnalle);
        tekstipaneeli.add(kokonaishinta);

        tilauspaneeli.add(tuotepaneeli, BorderLayout.WEST);
        tilauspaneeli.add(tekstipaneeli, BorderLayout.SOUTH);
        tilauspaneeli.add(tilaa, BorderLayout.EAST);
        tilauspaneeli.add(kuva2, BorderLayout.CENTER);

        // tapahtumankuuntelija tilausnapille
        tilaa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ohjain.teeTilaus();
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "ohjelmassa tapahtui virhe.");
                }
            }
        });

        /*
         OSTOSKORIPANEELI
         */
        korinSisalto = alkuKori;  
        puuttuvat = alkuPuuttuvat;  
        tekstiKorille = new JTextArea(korinSisalto, 20, 20);
        tekstiPuuttuville = new JTextArea(puuttuvat, 20, 20);
        korinTilanne = new JScrollPane(tekstiKorille);
        puuttuvienTilanne = new JScrollPane(tekstiPuuttuville);
        ostoskoripaneeli.add(korinTilanne);
        ostoskoripaneeli.add(puuttuvienTilanne);

        /*
         TALLENNUSPANEELI
         */
        // tehdään valintaruudut
        puuttuu = new JCheckBox("puuttuvat tilaukset");
        kori = new JCheckBox("korin sisältö");
        varasto = new JCheckBox("varaston tilanne");
        uusi = new JCheckBox("uusi tiedosto");
        vanha = new JCheckBox("vanha tiedosto");

        // tehdään paneeleita
        valintapaneeli = new JPanel();
        tiedostopaneeli = new JPanel(new GridLayout(1, 2));

        // lisätään komponentteja paneeleihin
        valintapaneeli.add(puuttuu);
        valintapaneeli.add(kori);
        valintapaneeli.add(varasto);
        valintapaneeli.setBorder(tallennusKehys);

        tiedostopaneeli.add(uusi);
        tiedostopaneeli.add(vanha);
        tiedostopaneeli.setBorder(fileKehys);

        // tehdään nappi tallennusta varten
        tallenna = new JButton("tallenna");

        tallennuspaneeli.add(valintapaneeli);
        tallennuspaneeli.add(tiedostopaneeli);
        tallennuspaneeli.add(tallenna);

//         tehdään kuuntelija tallennusnapille
        tallenna.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (puuttuu.isSelected()) {
                        ohjain.tallenna(uusi.isSelected(), "puuttuvat");
                    } else if (kori.isSelected()) {
                        ohjain.tallenna(uusi.isSelected(), "kori");
                    } else if (varasto.isSelected()) {
                        ohjain.tallenna(uusi.isSelected(), "varasto");
                    }
                    JOptionPane.showMessageDialog(null, "Tallennus onnistui.");
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "ohjelmassa tapahtui virhe.");
                }
            }
        });

        /*
         VALIKKORIVI
         */
        valikkorivi = new JMenuBar();
        valikko = new JMenu("Valikko");
        lopetus = new JMenuItem("lopeta");
        valikkorivi.add(valikko);
        lopetus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ohjain.lopeta();
            }
        });
        valikko.add(lopetus);

        sisaltopaneeli.add(valikkorivi);
        sisaltopaneeli.add(valilehtipaneeli);

        setSize(700, 500);
        setContentPane(sisaltopaneeli);
        setVisible(true);
    }

    // asiakkaan valitseman tuotteen indeksi listassa
    public int getTuotteenIndeksi() {
        return tuotteet.getSelectedIndex();
    }

    // luetaan asiakkaan syöttämä kappalemäärä
    public int getMaara() {
        return Integer.parseInt(maara.getText());
    }

    // asetetaan tilauksen kokonaishinta tekstilaatikkoon
    public void asetaHinta(double hinta) {
        kokonaishinta.setText(String.format("%.2f", hinta));
    }

    public void rekisteroiOhjain(EkaupanOhjain ohjain) {
        this.ohjain = ohjain;
    }
    
    public void asetaTekstiVarastolle(String teksti) {
        tekstiVarastolle.setText(teksti);
    }
    
    public void asetaTekstiKorille(String teksti) {
        tekstiKorille.setText(teksti);
    }
        
    public void asetaTekstiPuuttuville(String teksti) {
        tekstiPuuttuville.setText(teksti);
    }

}
